Realtime canvas and chat made at Red Panda Hackathon 11 February 2017
=====================================================================

Technology stack:
-----------------
1. Express
2. Socket.io
3. Jasmine for testing
4. Chai for assertions
5. Mongodb (monk package in node) for database

Aim of the project:
-------------------

1. The project should support real-time chat
2. The project should support real-time canvas