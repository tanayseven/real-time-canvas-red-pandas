var frisby = require('frisby');
var baseurl = 'http://localhost:3000';
frisby.create('Hello world is returned when you open the / page')
	.get(baseurl + '/')
	.expectStatus(200)
	.expectBodyContains('Hello World')
  .toss();