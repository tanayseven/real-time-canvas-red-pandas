var express = require('express');
var app = express();
const PORT = 3000;


app.get('/', function(req, res) {
	res.send('Hello World');
});

app.post('/login', function(req, res) {
	console.log(req.body);
});

app.listen(PORT, function() {
	console.log('Realtime chat and canvas is now running on http://0.0.0.0:' + PORT + '/');
})